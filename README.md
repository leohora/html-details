# Elemento HTML: details

A tag `<details>` especifica detalhes adicionais que o usuário pode abrir e fechar sob demanda.

Por padrão, é renderizado fechado. Quando aberto, ele se expande e exibe o conteúdo contido nele.

Assim, podemos agrupar vários deste elemento e estilizar o CSS de forma a criar um accordion.

## Redes Sociais
- [ ] [Instagram](https://www.instagram.com/leohoradev/)
- [ ] [YouTube](https://www.youtube.com/@LeoHoraDev)

## Contato
- [ ] leohoradev@gmail.com